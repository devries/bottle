# Bottle Base Docker Image

[![](https://images.microbadger.com/badges/version/devries/bottle.svg)](https://microbadger.com/images/devries/bottle "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/devries/bottle.svg)](https://microbadger.com/images/devries/bottle "Get your own image badge on microbadger.com")

The Dockerfile included in the repository creates an image based on the
ubuntu 16.04 (latest) or alpine linux (alpine_ image with pip and python dev
libraries installed. It also installs the [bottle
microframework](http://bottlepy.org) and the [gunicorn WSGI
server](http://gunicorn.org).

The image created will have a user names "apprunner" and group named
"apprunner" which are appropriate for running without root privileges. This
base image does not switch to that user, as the time and place for that is
appropriate for the derived image to determine.
